/**
 * Point-to-point synchronization.
 */
public struct BlackBoard[T](buffer:PlaceLocalHandle[Cell[T]]) {
    public def this(value:T) {
        property(PlaceLocalHandle.make[Cell[T]](PlaceGroup.WORLD, () => new Cell[T](value)));
    }
    /**
     * Synchronous put.
     */
    public operator this(target:Place)=(value:T) {
        at (target) {buffer()() = value;}
    }
    /**
     * Asynchronous put.
     */
    public def putAsync(target:Place, value:T) {
        at (target) async {buffer()() = value;}
    }
    /**
     * Synchronous get.
     */
    public operator this(target:Place) {
        return at(target) buffer()();
    }
    
    /**
     * Example.
     */
    public static def main(args:Rail[String]) {
        val board = BlackBoard[Long](0);
        var index:Long = 0;
        finish for (place in PlaceGroup.WORLD) {
            board.putAsync(place, index++);
        }
        for (place in PlaceGroup.WORLD) {
            Console.OUT.println(board(place));
        }
    }
}
