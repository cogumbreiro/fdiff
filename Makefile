X10 = x10
X10C = x10c
X10CC = x10c++
X10FLAGS = -VERBOSE_CHECKS

# probe how many cores it has
X10_NPLACES = $(shell grep processor /proc/cpuinfo | wc -l) 
ARGS = 16
export X10_NPLACES

all: run-managed

run-native: compile-native
	./fdiff $(ARGS)

run-managed: compile-managed
	$(X10) -np $(X10_NPLACES) fdiff $(ARGS)

compile-managed: fdiff.class

compile-native: fdiff

fdiff: fdiff.x10 BlackBoard.x10
	$(X10CC) $(X10FLAGS) fdiff.x10 -o fdiff

fdiff.class: fdiff.x10 BlackBoard.x10
	$(X10C) $(X10FLAGS) fdiff.x10

clean:
	rm -f fdiff*.class
