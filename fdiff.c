#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

#define MAX_SIZE 1024
#define MAX_ITER 100
#define MAX_ERR 0.0001

#include "fdiff.h"

void read_array(float data[], int size) {
  int i;

#ifndef _VCC_LIMITATIONS_
  for (i = 0; i < size; i++)
    scanf("%f", &data[i]);
#else
  for (i = 0; i < size; i++)
    data[i] = 31; 
#endif
}

void write_array(float data[], int size) {
  int i;
#ifndef _VCC_LIMITATIONS_
  for (i = 0; i < size; i++)
    printf("%f\n", data[i]);
#endif
}

void compute(float data[], float dataNIter[], int size) {
  int i;
  for (i = 1; i < size - 1; i++)
    dataNIter[i] = (data [i - 1] + 2 * data [i] + data [i + 1]) / 4;
}

float maxerror(float data[], float dataNIter[], int size) {
  float error = 0;
  int i;
  for (i = 1; i < size - 1; i++)
#ifndef _VCC_LIMITATIONS_
    error += fabs(dataNIter[i] - data [i]);
#else
    error = dataNIter[i] - data [i];
#endif
  return error;
}

int converged(float err) {
#ifndef _VCC_LIMITATIONS_
  return err < MAX_ERR;
#else
  return 1;
#endif
}

void swap(float x[], float y[], int size) {
  int i;
  for (i = 0; i < size; i++) {
    float swap_data = x[i];
    x[i] = y[i];
    y[i] = swap_data;
  }
}

int main(int argc, char** argv _ampi_arg_decl) {
  int np;              // Number of processes
  int me;              // Process rank

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &np);
  MPI_Comm_rank(MPI_COMM_WORLD, &me);

  _(assume _ampi_procs == 3)

  if (argc < 2)   // too few arguments
	return 1;
    
  int size = atoi(argv[1]);            // Global problem size

  if (size <= 0 || size >= MAX_SIZE - 2 || size % np != 0)  
    return 1;                          // returns in case of an invalid size

  _(ghost _type = type (me, size))

  float work[MAX_SIZE];                 // Initial input data (used only by rank 0)
  if (me == 0)
    read_array(work, size);

  // Scatter input data
  // Two extra slots in local buffer are required for boundary exchange
  int lsize = size / np;             // Local problem size
  float local[MAX_SIZE];                 // Local data buffer per process. 
  MPI_Scatter(work, lsize, MPI_FLOAT, &local[1], lsize, MPI_FLOAT, 0, MPI_COMM_WORLD);

  float globalerr = 999.0f;

  // Loop, until finite differences converge to a minimum error.
  int iter = 0;
  int lnbr = (np + me - 1) % np;         // Left neighbor rank
  int rnbr = (me + 1) % np;              // Right neighbor rank
  _(ghost \Type lb = loopBody(_type);)
  _(ghost \Type lc = next(_type);)
  while (!converged(globalerr) && iter < MAX_ITER) 
    _(writes &globalerr)
    _(writes \array_range(local, (unsigned) lsize + 2))
  {
    _(ghost _type = lb;)
    MPI_Status status;                   // MPI status data 

    if (me % 2 == 0) {
      MPI_Send(&local[1],       1, MPI_FLOAT, lnbr, 0, MPI_COMM_WORLD);
      MPI_Recv(&local[lsize+1], 1, MPI_FLOAT, rnbr, 0, MPI_COMM_WORLD, &status);
      MPI_Recv(&local[0],       1, MPI_FLOAT, lnbr, 0, MPI_COMM_WORLD, &status);
      MPI_Send(&local[lsize],   1, MPI_FLOAT, rnbr, 0, MPI_COMM_WORLD);
    } else {
      MPI_Recv(&local[lsize+1], 1, MPI_FLOAT, rnbr, 0, MPI_COMM_WORLD, &status);
      MPI_Send(&local[1],       1, MPI_FLOAT, lnbr, 0, MPI_COMM_WORLD);
      MPI_Send(&local[lsize],   1, MPI_FLOAT, rnbr, 0, MPI_COMM_WORLD);
      MPI_Recv(&local[0],       1, MPI_FLOAT, lnbr, 0, MPI_COMM_WORLD, &status);
    }
    float nextLocal[MAX_SIZE];             // Local data for the next iteration
    compute(local, nextLocal, lsize);
    float localerr = maxerror(local, nextLocal, lsize);
    MPI_Allreduce(&localerr, &globalerr, 1, MPI_FLOAT, MPI_MAX, MPI_COMM_WORLD); 
    swap (local, nextLocal, lsize + 2);
    iter++;
    _(assert _type == end())
  } 
   _(ghost _type = lc;)

  
  _(ghost \Type ct = choiceTrue(_type))
  _(ghost \Type cf = choiceFalse(_type))
  _(ghost \Type cc = next(_type))
  if (converged(globalerr)) {
      _(ghost _type = ct)  
    // Gather data at rank 0 for converged solution
    MPI_Gather(&local[1], lsize, MPI_FLOAT, work, lsize, MPI_FLOAT, 0, MPI_COMM_WORLD);

    if (me == 0) 
      write_array(work, size);
    _(assert _type == end())
  } else {
    _(ghost _type = cf)
#ifndef _VCC_LIMITATIONS_
    printf ("failed to converge after %d iterations!", MAX_ITER);
#endif
    _(assert _type == end())
  }
  _(ghost _type = cc)

  MPI_Finalize();

  return 0; 
}
