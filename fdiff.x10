import x10.util.Team;

class fdiff {
    val MAX_SIZE = 1024n;
    val MAX_ITER = 100n;
    val MAX_ERR = 0.0001f;

    def read_array(data: Rail[Float]) {
        for (i in data.range()) {
            data(i) = i; //scanf("%f");
        }
    }

    def write_array(data: Rail[Float]) {
        for (v in data) {
            Console.OUT.println(v);
        }
    }

    def compute(data:Rail[Float], dataNIter:Rail[Float]) {
        for (var i:Int = 1n; i < data.size - 1n; i++) {
            dataNIter(i) = (data(i - 1) + 2 * data(i) + data(i + 1)) / 4;
        }
    }

    def maxerror(data: Rail[Float], dataNIter:Rail[Float]):Float {
        var error:Float = 0.0f;
        for (var i:Int = 1n; i < data.size - 1n; i++) {
            error += Math.abs(dataNIter(i) - data (i));
        }
        return error;
    }

    def converged(err:Float):Boolean {
        return err < MAX_ERR;
    }

    def swap(x:Rail[Float], y:Rail[Float]) {
        for (i in x.range()) {
            val swap_data = x(i);
            x(i) = y(i);
            y(i) = swap_data;
        }
    }
    
    def run(args:Rail[String]) {
        val buffer = BlackBoard[Float](0.0f);
        val team = Team.WORLD;
        val master = new Place(0);
        val np:Int = PlaceGroup.WORLD.size() as Int; // Number of processes
        if (args.size < 1) {   // too few arguments
            Console.ERR.println("too few arguments");
            return;
        }

        val size = Int.parse(args(0)); // Global problem size

        if (size <= 0n || size >= MAX_SIZE - 2 || size % np != 0n)  {
            Console.ERR.println("invalid size=" + size + ": 0 <= size < " + (MAX_SIZE - 2) + " and size % " + np + " = 0");
            return; // returns in case of an invalid size
        }
        val work = PlaceLocalHandle.makeFlat[Rail[Float]](PlaceGroup.WORLD, () => new Rail[Float](MAX_SIZE));
        val local = PlaceLocalHandle.makeFlat[Rail[Float]](PlaceGroup.WORLD, () => new Rail[Float](MAX_SIZE));
        val lsize = size / np; // Local problem size
        // Initial input data (used only by rank 0)
        read_array(work());
        Util.asyncScatter[Float](work(), 0, PlaceGroup.WORLD, local, 1, lsize as Long);
        val c = Clock.make();
        for (place in PlaceGroup.WORLD) at (place) async clocked(c) {
            val me:Int = Runtime.hereInt(); // Process rank
            Console.OUT.println("Start " + me);

            // Scatter input data
            // Two extra slots in local buffer are required for boundary exchange
            var globalerr:Float = 999.0f;

            // Loop, until finite differences converge to a minimum error.
            var iter:Int = 0n;
            val lnbr = new Place((np + me - 1) % np);         // Left neighbor rank
            val rnbr = new Place((me + 1) % np);              // Right neighbor rank
            while (!converged(globalerr) && iter < MAX_ITER) {
                if (me % 2 == 0) {
                    // Phase 0:
                    //MPI_Send(&local[1], 1, MPI_FLOAT, lnbr, 0, MPI_COMM_WORLD);
                    buffer(lnbr) = local()(1);
                    Clock.advanceAll();
                    // Phase 1:
                    //MPI_Recv(&local[lsize+1], 1, MPI_FLOAT, rnbr, 0, MPI_COMM_WORLD, &status);
                    local()(lsize + 1) = buffer(rnbr);
                    Clock.advanceAll();
                    // Phase 2: (other write)
                    Clock.advanceAll();
                    // Phase 3: (other write)
                    Clock.advanceAll();
                    // Phase 4:
                    //MPI_Recv(&local[0], 1, MPI_FLOAT, lnbr, 0, MPI_COMM_WORLD, &status);
                    local()(0) = buffer(lnbr);
                    Clock.advanceAll();
                    // Phase 5:
                    //MPI_Send(&local[lsize], 1, MPI_FLOAT, rnbr, 0, MPI_COMM_WORLD);
                    buffer(rnbr) = local()(lsize);
                    Clock.advanceAll();
                } else {
                    // Phase 0: (other write)
                    Clock.advanceAll();
                    // Phase 1: 
                    //MPI_Recv(&local[lsize+1], 1, MPI_FLOAT, rnbr, 0, MPI_COMM_WORLD, &status);
                    local()(lsize + 1) = buffer(rnbr);
                    Clock.advanceAll();
                    // Phase 2:
                    //MPI_Send(&local[1], 1, MPI_FLOAT, lnbr, 0, MPI_COMM_WORLD);
                    buffer(lnbr) = local()(1);
                    Clock.advanceAll();
                    // Phase 3:
                    //MPI_Send(&local[lsize], 1, MPI_FLOAT, rnbr, 0, MPI_COMM_WORLD);
                    buffer(rnbr) = local()(lsize);
                    Clock.advanceAll();
                    // Phase 4:
                    //MPI_Recv(&local[0], 1, MPI_FLOAT, lnbr, 0, MPI_COMM_WORLD, &status);
                    buffer(lnbr) = local()(0);
                    Clock.advanceAll();
                    // Phase 5: (other write)
                    Clock.advanceAll();
                }
                
                val nextLocal = new Rail[Float](MAX_SIZE); // Local data for the next iteration
                compute(local(), nextLocal);
                val localerr = maxerror(local(), nextLocal);
                //MPI_Allreduce(&localerr, &globalerr, 1, MPI_FLOAT, MPI_MAX, MPI_COMM_WORLD);
                //globalerr = team.allreduce(localerr, Team.MAX);
                swap(local(), nextLocal);
                
                iter++;
            }
            if (converged(globalerr)) {
                // Gather data at rank 0 for converged solution
                //MPI_Gather(&local[1], lsize, MPI_FLOAT, work, lsize, MPI_FLOAT, 0, MPI_COMM_WORLD);
                // TODO
                // if (me == 0) 
                // write_array(work, size);
            } else {
                if (me == 0n) {
                    Console.OUT.println("failed to converge after " + MAX_ITER + " iterations!");
                }
            }
        }
    }

    public static def main(args:Rail[String]) {
        new fdiff().run(args);
    }
}
