import x10.util.Team;

class error {
    public static def main(args:Rail[String]) {
        PlaceGroup.WORLD.broadcastFlat(()=>{
            val work = new Rail[Float](1024);
            val local = new Rail[Float](1024);
            Team.WORLD.scatter(new Place(0), work, 0, local, 1, 4);
        });
    }
}
