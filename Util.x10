/**
 * Communication operations.
 */
public struct Util {
    //scatter(Place root, Rail<T> src, Long src_off, Rail<T> dst, Long dst_off, Long count) 
    public static def asyncScatter[T](src:Rail[T], src_off:Long, group:PlaceGroup, dst:PlaceLocalHandle[Rail[T]], dst_off:Long, count:Long) {
        val global_src = GlobalRail[T](src);
        for (place in group) {
            at (place) async {
                Rail.asyncCopy(global_src, src_off, dst(), dst_off, count);
            }
        }
    }
    public static def asyncGather[T](src:PlaceLocalHandle[Rail[T]], src_off:Long, group:PlaceGroup, dst:Rail[T], dst_off:Long, count:Long) {
        val global_dst = GlobalRail[T](dst);
        var index:Long = dst_off;
        for (place in group) {
            val curr_index = index;
            at (place) async {
                Rail.asyncCopy(src(), src_off, global_dst, curr_index, count);
            }
            index += count;
        }
    }
    
    public static def main(args:Rail[String]) {
        val size = args.size;
        val blockSize = 2;
        val src = new Rail[Long](PlaceGroup.WORLD.size * blockSize, (x:Long) => x);
        val dst = PlaceLocalHandle.makeFlat[Rail[Long]](PlaceGroup.WORLD, () => new Rail[Long](blockSize));
        finish asyncScatter[Long](src, 0, PlaceGroup.WORLD, dst, 0, blockSize); // 2 bytes
        for (place in PlaceGroup.WORLD) {
            Console.OUT.println(dst());
        }
        finish asyncGather[Long](dst, 0, PlaceGroup.WORLD, src, 0, blockSize);
        Console.OUT.println(src);
    }
}
